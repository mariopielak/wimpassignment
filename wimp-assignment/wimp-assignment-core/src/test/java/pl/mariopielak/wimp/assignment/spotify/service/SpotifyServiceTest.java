package pl.mariopielak.wimp.assignment.spotify.service;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;
import pl.mariopielak.wimp.assignment.api.dto.Track;

@Test
public class SpotifyServiceTest {

	private SpotifyService objectUnderTest = new SpotifyService();

	@Test
	public void shouldCallSpotifyApiAndGetSomeResults() {
		// given
		List<EchonestTrack> echonestTracks = prepareEchonestTracks();

		// when
		List<Track> tracks = objectUnderTest.findTracks(echonestTracks);

		// then
		Assert.assertEquals(tracks.size(), 2);
	}

	private List<EchonestTrack> prepareEchonestTracks() {
		List<EchonestTrack> echonestTracks = new ArrayList<>();
		echonestTracks.add(new EchonestTrack("3L7BcXHCG8uT92viO6Tikl"));
		echonestTracks.add(new EchonestTrack("5k5Ro7qJN8cBwclhlG4xsb"));
		return echonestTracks;
	}

}
