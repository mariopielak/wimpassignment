package pl.mariopielak.wimp.assignment.service;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pl.mariopielak.wimp.assignment.api.dto.Playlist;
import pl.mariopielak.wimp.assignment.api.dto.PlaylistQueryParams;
import pl.mariopielak.wimp.assignment.api.dto.builder.PlaylistQueryParamsBuilder;

@Test
public class WimpAssignmentPlaylistServiceTest {

	private static final int TEST_DEFAULT_RESULT_SIZE = 10;

	private WimpAssignmentPlaylistService objectUnderTest = new WimpAssignmentPlaylistService();

	@Test(dataProvider = "playlistQueryParams")
	public void shouldCallEchonestAndSpotifyApi(String text, String resultSize, boolean fasterOption, int expectedSize) {
		// given
		PlaylistQueryParams params = PlaylistQueryParamsBuilder.create().withText(text).withResultSize(resultSize).withFasterOption(fasterOption).build();

		// when
		Playlist playlist = objectUnderTest.findPlaylist(params);

		// then
		Assert.assertEquals(playlist.getTracks().size(), expectedSize);
	}

	@DataProvider
	private Object[][] playlistQueryParams() {
		return new Object[][] {
				{ "jazz", "6", false, 6 },
				{ "genreNotExists", "1", false , TEST_DEFAULT_RESULT_SIZE },
				{ "classic", "5", true, 5 }
			};
	}

}
