package pl.mariopielak.wimp.assignment.echonest.service;

import java.util.List;
import java.util.Optional;

import org.testng.Assert;
import org.testng.annotations.Test;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;

@Test
public class EchonestServiceTest {

	private static final int TEST_DEFAULT_RESULT_SIZE = 10;

	private EchonestServiceDeprecated objectUnderTest = new EchonestServiceDeprecated();

	@Test
	public void shoudlCallEchonestApiAndGetSomeResultsInDefaultMode() {
		// given
		// when
		List<EchonestTrack> echonestTracks = objectUnderTest.findTracks("jazz", Optional.empty(), false);

		// then
		Assert.assertEquals(echonestTracks.size(), TEST_DEFAULT_RESULT_SIZE);
	}

	@Test
	public void shouldCallEchonestApiAndGetSomeResultsInFasterMode() {
		// given
		// when
		List<EchonestTrack> echonestTracks = objectUnderTest.findTracks("jazz", Optional.of("5"), true);

		// then
		Assert.assertEquals(echonestTracks.size(), 5);		
	}

}
