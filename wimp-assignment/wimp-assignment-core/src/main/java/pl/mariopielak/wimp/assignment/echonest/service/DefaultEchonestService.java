package pl.mariopielak.wimp.assignment.echonest.service;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

public class DefaultEchonestService extends AbstractEchonestService implements EchonestService {

	private static final String PLAYLIST_FIND_METHOD = "v4/playlist/basic";

	@Override
	public List<EchonestTrack> findTracks(String genre, Optional<String> resultSize) {
		return findTracks(PLAYLIST_FIND_METHOD, prepareMapWithQueryParametersForDefaultOption(genre, resultSize));
	}

	private MultivaluedMap<String, String> prepareMapWithQueryParametersForDefaultOption(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = prepareMapWithCommonParameters(genre, resultSize);
		queryParamsMap.add("genre", genre);
		queryParamsMap.add("type", "genre-radio");
		return queryParamsMap;
	}
}
