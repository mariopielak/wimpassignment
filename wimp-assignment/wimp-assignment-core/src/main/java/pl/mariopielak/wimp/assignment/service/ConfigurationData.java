package pl.mariopielak.wimp.assignment.service;

import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import pl.mariopielak.wimp.assignment.api.dto.Playlist;

import com.fasterxml.jackson.databind.ObjectMapper;

//FIXME [mariopielak] fix to enum singleton?
public class ConfigurationData {

	private static PropertiesConfiguration CONFIG;

	private static Playlist DEFAULT_PLAYLIST;

	private ConfigurationData() {}

	static {
		try {
			CONFIG = new PropertiesConfiguration("configuration.properties");
			DEFAULT_PLAYLIST = new ObjectMapper().readValue(ConfigurationData.class.getResourceAsStream("/defaultPlaylist.json"), Playlist.class);
		} catch (ConfigurationException | IOException e) {
			throw new IllegalStateException("Necessary file are not provided!!!");
		}
	}

	public static long getTimeoutForPlaylistSearchOperation() {
		return CONFIG.getLong("timeout.playlist.search.operation");
	}

	public static String getEchonestApiKey() {
		return CONFIG.getString("echonest.apikey");
	}

	public static Playlist getDefaultPlaylist() {
        return DEFAULT_PLAYLIST;
	}

	public static String getDefaultResultSize() {
		return CONFIG.getString("result.size");
	}

}
