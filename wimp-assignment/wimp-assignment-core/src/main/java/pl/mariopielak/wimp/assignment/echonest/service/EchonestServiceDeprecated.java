package pl.mariopielak.wimp.assignment.echonest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONArray;
import org.json.JSONObject;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;
import pl.mariopielak.wimp.assignment.service.ConfigurationData;
import pl.mariopielak.wimp.assignment.service.WimpAssignmentHttpManager;

import com.sun.jersey.core.util.MultivaluedMapImpl;

public class EchonestServiceDeprecated {

	private static final String ECHONEST_ROOT_PATH = "http://developer.echonest.com/api";

	private static final String SONG_FIND_METHOD = "v4/song/search";

	private static final String PLAYLIST_FIND_METHOD = "v4/playlist/basic";

	private static final String SPOTIFY_TRACK = "spotify:track:";

	private WimpAssignmentHttpManager wimpAssignmentHttpManager = new WimpAssignmentHttpManager();

	public List<EchonestTrack> findTracks(String genre, Optional<String> resultSize, boolean fasterOption) {
		return fasterOption ? 
				findTracks(SONG_FIND_METHOD, prepareMapWithQueryParametersForFasterOption(genre, resultSize)) :
				findTracks(PLAYLIST_FIND_METHOD, prepareMapWithQueryParametersForDefaultOption(genre, resultSize));
	}

	private List<EchonestTrack> findTracks(String path, MultivaluedMap<String, String> queryParameters) {
		String resultAsString = wimpAssignmentHttpManager.callHttpGetMehodAndParseToString(ECHONEST_ROOT_PATH, path, queryParameters);
		JSONObject rootJsonObject = new JSONObject(resultAsString);
		JSONArray songsJsonArray = rootJsonObject.getJSONObject("response").getJSONArray("songs");
		List<EchonestTrack> echonestTracks = new ArrayList<>();
		for(int index = 0; index < songsJsonArray.length(); index++) {
			echonestTracks.add(createEchonextTrack(songsJsonArray.getJSONObject(index)));
		}
		return echonestTracks;
	}

	private EchonestTrack createEchonextTrack(JSONObject songJsonObject) {
		String id = songJsonObject.getJSONArray("tracks").getJSONObject(0).getString("foreign_id");
		return new EchonestTrack(id.substring(SPOTIFY_TRACK.length()));
	}

	private MultivaluedMap<String, String> prepareMapWithQueryParametersForFasterOption(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = prepareMapWithCommonParameters(genre, resultSize);
		queryParamsMap.add("style", genre);
		return queryParamsMap;
	}

	private MultivaluedMap<String, String> prepareMapWithQueryParametersForDefaultOption(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = prepareMapWithCommonParameters(genre, resultSize);
		queryParamsMap.add("genre", genre);
		queryParamsMap.add("type", "genre-radio");
		return queryParamsMap;
	}

	protected MultivaluedMap<String, String> prepareMapWithCommonParameters(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = new MultivaluedMapImpl();
		queryParamsMap.add("api_key", ConfigurationData.getEchonestApiKey());
		queryParamsMap.add("format", "json");
		queryParamsMap.add("results", resultSize.orElse(ConfigurationData.getDefaultResultSize()));
		queryParamsMap.add("bucket", "id:spotify");
		queryParamsMap.add("bucket", "tracks");
		queryParamsMap.add("limit", "true");
		return queryParamsMap;
	}

}
