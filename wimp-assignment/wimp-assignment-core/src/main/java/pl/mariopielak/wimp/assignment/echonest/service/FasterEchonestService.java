package pl.mariopielak.wimp.assignment.echonest.service;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

public class FasterEchonestService extends AbstractEchonestService implements EchonestService {

	private static final String SONG_FIND_METHOD = "v4/song/search";

	@Override
	public List<EchonestTrack> findTracks(String genre, Optional<String> resultSize) {
		return findTracks(SONG_FIND_METHOD, prepareMapWithQueryParametersForFasterOption(genre, resultSize));
	}

	private MultivaluedMap<String, String> prepareMapWithQueryParametersForFasterOption(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = prepareMapWithCommonParameters(genre, resultSize);
		queryParamsMap.add("style", genre);
		return queryParamsMap;
	}

}
