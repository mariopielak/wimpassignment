package pl.mariopielak.wimp.assignment.algorithm.service;

import pl.mariopielak.wimp.assignment.api.dto.TextSimilarityAlgorithmResult;
import pl.mariopielak.wimp.assignment.api.service.TextSimilarityAlgorithm;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

//FIXME it's fake implementation for wimp assignment needs
public class TFIDFSimilarityAlgorithmFakeImpl implements TextSimilarityAlgorithm {

	private static final String WHITE_SPACE_REGEX = "\\s+";

	private static final int FAKE_COUNT_NUMBER = 1;

	@Override
	public TextSimilarityAlgorithmResult analyzeTextAndFindDominantKey(String text) {
		String textData[] = text.split(WHITE_SPACE_REGEX);
		return new TextSimilarityAlgorithmResult(getMapWithFakeAnalysisData(textData), textData[0]);
	}

	private Map<String, Integer> getMapWithFakeAnalysisData(String[] textData) {
		return Arrays.stream(textData).collect(Collectors.toMap(s -> s, s -> FAKE_COUNT_NUMBER));
	}

}
