package pl.mariopielak.wimp.assignment.echonest.service;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;

import java.util.List;
import java.util.Optional;

public interface EchonestService {

	List<EchonestTrack> findTracks(String genre, Optional<String> resultSize);

}
