package pl.mariopielak.wimp.assignment.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MultivaluedMap;

public class WimpAssignmentHttpManager {

	private static final String APPLICATION_JSON = "application/json";

	private Client client;

	public WimpAssignmentHttpManager() {
		client = Client.create();
	}

	public String callHttpGetMehodAndParseToString(String resource, String path, MultivaluedMap<String, String> queryParams) {
		WebResource webResource = client.resource(resource);
		ClientResponse response = webResource.path(path).queryParams(queryParams).accept(APPLICATION_JSON).get(ClientResponse.class);
		return response.getEntity(String.class);
	}

}
