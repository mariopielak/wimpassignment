package pl.mariopielak.wimp.assignment.service;

import pl.mariopielak.wimp.assignment.algorithm.service.TFIDFSimilarityAlgorithmFakeImpl;
import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;
import pl.mariopielak.wimp.assignment.api.dto.Playlist;
import pl.mariopielak.wimp.assignment.api.dto.PlaylistQueryParams;
import pl.mariopielak.wimp.assignment.api.dto.TextSimilarityAlgorithmResult;
import pl.mariopielak.wimp.assignment.api.dto.Track;
import pl.mariopielak.wimp.assignment.api.service.PlaylistService;
import pl.mariopielak.wimp.assignment.api.service.TextSimilarityAlgorithm;
import pl.mariopielak.wimp.assignment.echonest.service.DefaultEchonestService;
import pl.mariopielak.wimp.assignment.echonest.service.EchonestService;
import pl.mariopielak.wimp.assignment.echonest.service.FasterEchonestService;
import pl.mariopielak.wimp.assignment.spotify.service.SpotifyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class WimpAssignmentPlaylistService implements PlaylistService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private SpotifyService spotifyService = new SpotifyService();

	private EchonestService defaultEchonestService = new DefaultEchonestService();

	private EchonestService fasterEchonestService = new FasterEchonestService();

	private TextSimilarityAlgorithm textSimilarityAlgorithm = new TFIDFSimilarityAlgorithmFakeImpl();

	public Playlist findPlaylist(final PlaylistQueryParams playlistQueryParams) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<Playlist> future = executor.submit(() -> findPlaylistWithTimeout(playlistQueryParams));
		try {
			return future.get(ConfigurationData.getTimeoutForPlaylistSearchOperation(), TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
			logger.error("Timeout problem!", e);
			future.cancel(true);
		} catch (InterruptedException | ExecutionException e) {
			logger.error("Sth wrong happend inside findplaylist thread", e);
		} finally {
			executor.shutdownNow();
		}
		return ConfigurationData.getDefaultPlaylist();
	}

	private Playlist findPlaylistWithTimeout(PlaylistQueryParams playlistQueryParams) {
		TextSimilarityAlgorithmResult textSimilarityAlgorithmResult = textSimilarityAlgorithm.analyzeTextAndFindDominantKey(playlistQueryParams.getText());
		EchonestService echonestService = playlistQueryParams.isFasterOption() ? fasterEchonestService : defaultEchonestService;
		List<EchonestTrack> echonestTracks = echonestService.findTracks(textSimilarityAlgorithmResult.getDominantKey(), playlistQueryParams.getResultSize());
		List<Track> spotifyTracks = spotifyService.findTracks(echonestTracks);
		return new Playlist(textSimilarityAlgorithmResult.getAnalysisTextResults(), spotifyTracks);
	}

}
