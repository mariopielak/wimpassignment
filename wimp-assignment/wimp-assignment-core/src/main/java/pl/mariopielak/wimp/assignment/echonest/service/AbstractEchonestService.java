package pl.mariopielak.wimp.assignment.echonest.service;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;
import pl.mariopielak.wimp.assignment.service.ConfigurationData;
import pl.mariopielak.wimp.assignment.service.WimpAssignmentHttpManager;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MultivaluedMap;

public abstract class AbstractEchonestService {

	private static final String ECHONEST_ROOT_PATH = "http://developer.echonest.com/api";

	private static final String SPOTIFY_TRACK = "spotify:track:";

	private WimpAssignmentHttpManager wimpAssignmentHttpManager = new WimpAssignmentHttpManager();

	protected MultivaluedMap<String, String> prepareMapWithCommonParameters(String genre, Optional<String> resultSize) {
		MultivaluedMap<String, String> queryParamsMap = new MultivaluedMapImpl();
		queryParamsMap.add("api_key", ConfigurationData.getEchonestApiKey());
		queryParamsMap.add("format", "json");
		queryParamsMap.add("results", resultSize.orElse(ConfigurationData.getDefaultResultSize()));
		queryParamsMap.add("bucket", "id:spotify");
		queryParamsMap.add("bucket", "tracks");
		queryParamsMap.add("limit", "true");
		return queryParamsMap;
	}

	protected List<EchonestTrack> findTracks(String path, MultivaluedMap<String, String> queryParameters) {
		String resultAsString = wimpAssignmentHttpManager.callHttpGetMehodAndParseToString(ECHONEST_ROOT_PATH, path, queryParameters);
		JSONObject rootJsonObject = new JSONObject(resultAsString);
		JSONArray songsJsonArray = rootJsonObject.getJSONObject("response").getJSONArray("songs");
		List<EchonestTrack> echonestTracks = new ArrayList<>();
		for(int index = 0; index < songsJsonArray.length(); index++) {
			echonestTracks.add(createEchonextTrack(songsJsonArray.getJSONObject(index)));
		}
		return echonestTracks;
	}

	protected EchonestTrack createEchonextTrack(JSONObject songJsonObject) {
		String id = songJsonObject.getJSONArray("tracks").getJSONObject(0).getString("foreign_id");
		return new EchonestTrack(id.substring(SPOTIFY_TRACK.length()));
	}
}
