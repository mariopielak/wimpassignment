package pl.mariopielak.wimp.assignment.spotify.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import pl.mariopielak.wimp.assignment.api.dto.EchonestTrack;
import pl.mariopielak.wimp.assignment.api.dto.Track;
import pl.mariopielak.wimp.assignment.service.WimpAssignmentHttpManager;

import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SpotifyService {

	private static final String SPOTIFY_ROOT_PATH = "https://api.spotify.com";

	private static final String TRACKS_FIND_METHOD = "/v1/tracks";

	private static final String TRACKS_DELIMITER = ",";

	private WimpAssignmentHttpManager wimpAssignmentHttpManager = new WimpAssignmentHttpManager();

	public List<Track> findTracks(List<EchonestTrack> echonestTracks) {
		String data = wimpAssignmentHttpManager.callHttpGetMehodAndParseToString(SPOTIFY_ROOT_PATH, TRACKS_FIND_METHOD, prepareMapWithQueryParameters(echonestTracks));
		JSONObject rootJsonObject = new JSONObject(data);
		JSONArray tracksJsonArray = rootJsonObject.getJSONArray("tracks");
		List<Track> tracks = new ArrayList<>();
		for (int index = 0; index < tracksJsonArray.length(); index++) {
			tracks.add(createTrack(tracksJsonArray.getJSONObject(index)));
		}
		return tracks;
	}

	private Track createTrack(JSONObject songJsonObject) {
		String snippetUrl = songJsonObject.optString("preview_url");
		String title = songJsonObject.optString("name");
		String artist = tryToGetArtist(songJsonObject);
		return new Track(snippetUrl, title, artist);
	}

	private String tryToGetArtist(JSONObject songJsonObject) {
		JSONArray artistsJsonArray = songJsonObject.optJSONArray("artists");
		if (artistsJsonArray != null) {
			return artistsJsonArray.getJSONObject(0).optString("name");
		}
		return StringUtils.EMPTY;
	}

	private MultivaluedMap<String, String> prepareMapWithQueryParameters(
			List<EchonestTrack> echonestTracks) {
		MultivaluedMap<String, String> queryParamsMap = new MultivaluedMapImpl();
		String spotifyTrackIds = echonestTracks.stream()
				.map(p -> p.getSpotifyId())
				.collect(Collectors.joining(TRACKS_DELIMITER));
		queryParamsMap.add("ids", spotifyTrackIds);
		return queryParamsMap;
	}

}
