package pl.mariopielak.wimp.assignment.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pl.mariopielak.wimp.assignment.api.dto.Playlist;
import pl.mariopielak.wimp.assignment.api.dto.builder.PlaylistQueryParamsBuilder;
import pl.mariopielak.wimp.assignment.api.service.PlaylistService;
import pl.mariopielak.wimp.assignment.service.WimpAssignmentPlaylistService;

@Path(WimpAssignmentResource.ROOT_PATH)
@Produces(MediaType.APPLICATION_JSON)
public class WimpAssignmentResource {

	protected static final String ROOT_PATH = "wimp-assignment";

	private PlaylistService playlistService = new WimpAssignmentPlaylistService();

	@GET
	@Path("/findPlaylist")
	public Playlist findPlaylist(@QueryParam("text") String text, @QueryParam("resultSize") String resultSize, @QueryParam("faster") boolean faster) {
		return playlistService.findPlaylist(PlaylistQueryParamsBuilder.create().withText(text).withResultSize(resultSize).build());
	}

}
