package pl.mariopielak.wimp.assignment.rest.configuration;

import pl.mariopielak.wimp.assignment.rest.WimpAssignmentResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class WimpAssignmentApplication extends Application<WimpAssignmentConfiguration> {

	private static final String RESOURCE_PATH = "wimp-assignment";

	public static void main(String[] args) throws Exception {
		new WimpAssignmentApplication().run(args);
	}

	@Override
	public String getName() {
		return RESOURCE_PATH;
	}

	@Override
	public void initialize(Bootstrap<WimpAssignmentConfiguration> bootstrap) {
		// nop
	}

	@Override
	public void run(WimpAssignmentConfiguration configuration, Environment environment) throws Exception {
		final WimpAssignmentResource resource = new WimpAssignmentResource();
		environment.jersey().register(resource);
	}

}
