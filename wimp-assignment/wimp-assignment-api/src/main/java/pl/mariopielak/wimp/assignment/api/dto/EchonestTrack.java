package pl.mariopielak.wimp.assignment.api.dto;

public class EchonestTrack {

	private String spotifyId;

	public EchonestTrack(String spotifyId) {
		this.spotifyId = spotifyId;
	}

	public String getSpotifyId() {
		return spotifyId;
	}

}
