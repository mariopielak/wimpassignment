package pl.mariopielak.wimp.assignment.api.service;

import pl.mariopielak.wimp.assignment.api.dto.TextSimilarityAlgorithmResult;

public interface TextSimilarityAlgorithm {

	TextSimilarityAlgorithmResult analyzeTextAndFindDominantKey(String text);
}
