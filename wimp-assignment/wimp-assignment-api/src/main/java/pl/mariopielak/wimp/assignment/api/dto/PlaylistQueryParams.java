package pl.mariopielak.wimp.assignment.api.dto;

import java.util.Optional;

public class PlaylistQueryParams {

	private String text;

	private Optional<String> resultSize;

	private boolean fasterOption;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Optional<String> getResultSize() {
		return resultSize;
	}

	public void setResultSize(Optional<String> resultSize) {
		this.resultSize = resultSize;
	}

	public boolean isFasterOption() {
		return fasterOption;
	}

	public void setFasterOption(boolean fasterOption) {
		this.fasterOption = fasterOption;
	}

}
