package pl.mariopielak.wimp.assignment.api.dto;

public class Track {

	private String artist;

	private String title;

	private String snippetUrl;

	public Track(String snippetUrl, String title, String artist) {
		this.snippetUrl = snippetUrl;
		this.title = title;
		this.artist = artist;
	}

	public Track() {
		// deserialize json
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippetUrl() {
		return snippetUrl;
	}

	public void setSnippetUrl(String snippetUrl) {
		this.snippetUrl = snippetUrl;
	}

}
