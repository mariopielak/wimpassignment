package pl.mariopielak.wimp.assignment.api.dto.builder;

import pl.mariopielak.wimp.assignment.api.dto.PlaylistQueryParams;

import java.util.Optional;

public class PlaylistQueryParamsBuilder {

	private PlaylistQueryParams playlistQueryParams = new PlaylistQueryParams();

	public static PlaylistQueryParamsBuilder create() {
		return new PlaylistQueryParamsBuilder();
	}

	public PlaylistQueryParamsBuilder withText(String text) {
		playlistQueryParams.setText(text);
		return this;
	}

	public PlaylistQueryParamsBuilder withResultSize(String resultSize) {
		playlistQueryParams.setResultSize(Optional.ofNullable(resultSize));
		return this;
	}

	public PlaylistQueryParamsBuilder withFasterOption(boolean fasterOption) {
		playlistQueryParams.setFasterOption(fasterOption);
		return this;
	}

	public PlaylistQueryParams build() {
		return playlistQueryParams;
	}

}
