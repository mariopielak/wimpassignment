package pl.mariopielak.wimp.assignment.api.dto;

import java.util.List;
import java.util.Map;

public class Playlist {

	private Map<String, Integer> analysisTextResults;

	private List<Track> tracks;

	public Playlist() {
		// deserialize
	}

	public Playlist(Map<String, Integer> analysisTextResults, List<Track> tracks) {
		this.analysisTextResults = analysisTextResults;
		this.tracks = tracks;
	}

	public Map<String, Integer> getAnalysisTextResults() {
		return analysisTextResults;
	}

	public void setAnalysisTextResults(Map<String, Integer> analysisTextResults) {
		this.analysisTextResults = analysisTextResults;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

}
