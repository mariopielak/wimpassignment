package pl.mariopielak.wimp.assignment.api.service;

import pl.mariopielak.wimp.assignment.api.dto.Playlist;
import pl.mariopielak.wimp.assignment.api.dto.PlaylistQueryParams;

public interface PlaylistService {

	public Playlist findPlaylist(PlaylistQueryParams playlistQueryParams);
}
