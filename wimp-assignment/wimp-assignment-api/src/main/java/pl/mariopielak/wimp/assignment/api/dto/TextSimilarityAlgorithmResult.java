package pl.mariopielak.wimp.assignment.api.dto;

import java.util.Map;

public class TextSimilarityAlgorithmResult {

	private Map<String, Integer> analysisTextResults;

	private String dominantKey;
	
	public TextSimilarityAlgorithmResult(Map<String, Integer> analysisTextResults, String dominantKey) {
		this.analysisTextResults = analysisTextResults;
		this.dominantKey = dominantKey;
	}

	public Map<String, Integer> getAnalysisTextResults() {
		return analysisTextResults;
	}

	public String getDominantKey() {
		return dominantKey;
	}

}
